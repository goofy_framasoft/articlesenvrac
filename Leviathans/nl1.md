# Histoire d'une conversion capitaliste

Dans ce premier chapitre, je tente de synthétiser les transformations des utopies numériques des débuts de l'économie informatique vers ce que S. Zuboff nomme le «&nbsp;capitalisme de surveillance&nbsp;». Dans cette histoire, le logiciel libre apparaît non seulement comme un élément critique présent dès les premiers âges de cette conversion capitaliste, mais aussi comme le moyen de faire valoir la primauté de nos libertés individuelles face aux comportements imposés par un nouvel ordre numérique. La dégooglisation d'Internet n'est plus un souhait, c'est un impératif&nbsp;!


## Techno-capitalisme

Longtemps nous avons cru que le versant obscur du capitalisme était le
monopole. En écrasant le marché, en pratiquant des prix arbitraires, en
contrôlant les flux et la production à tous les niveaux, un monopole est
un danger politique. Devant la tendance monopolistique de certaines
entreprises, le gouvernement américain a donc très tôt mis en place une
stratégie juridique visant à limiter les monopoles. C'est le [Sherman
Anti-Trust Act](https://fr.wikipedia.org/wiki/Sherman_Antitrust_Act) du 2 juillet 1890, ce qui ne rajeunit pas l'économie moderne. Avec cette loi dont beaucoup de pays ont adopté les principes,
le fameux droit de la concurrence a modelé les économies à une échelle
mondiale. Mais cela n'a pas pour autant empêché l'apparition de
monopoles. Parmi les entreprises ayant récemment fait l'objet de
poursuites au nom de lois anti-trust sur le territoire américain et
[en Europe](https://en.wikipedia.org/wiki/Microsoft_Corp_v_Commission), on peut citer Microsoft, qui s'en est toujours tiré à bon compte (compte tenu de son placement financier).

Que les procès soient gagnés ou non, les firmes concernées dépensent des
millions de dollars pour leur défense et, d'une manière ou d'une autre,
trouvent toujours un moyen de contourner les procédures. C'est le cas de
Google qui, en août 2015, devant l'obésité due à ses multiples
activités, a préféré éclater en plusieurs sociétés regroupées sous une
sorte de Holding nommée Alphabet. Crainte de se voir poursuivie au nom
de lois anti-trust&nbsp;? non, du moins ce n'est pas le premier objectif[@obs2015]&nbsp;: ce qui a motivé cet éclatement, c'est de pouvoir rendre plus claires ses différentes activités pour les investisseurs.


![Alphabet. CC-by-sa, Framatophe](imgs/alphabet.png)

Investir dans les sociétés qui composent Alphabet, ce serait donc leur
permettre à toutes de pouvoir initier de nouveaux projets. N'en a-t-il
toujours pas été ainsi, dans le monde capitaliste&nbsp;? Investir, innover,
produire pour le bien de l'humanité. Dans ce cas, quel danger
représenterait le capitalisme&nbsp;? aucun. Dans le monde des technologies
numériques, au contraire, il constitue le moteur idéal pour favoriser
toute forme de progrès social, technique et même politique. Dans nos
beaux pays industriels avancés (hum&nbsp;!) nous n'en sommes plus au temps
des mines à charbon et des revendications sociales [à la Zola](https://fr.wikisource.org/wiki/Germinal)&nbsp;: tout cela est d'un autre âge, celui où le capitalisme était aveugle. Place au
capitalisme éclairé.

Convaincu&nbsp;? pas vraiment n'est-ce pas&nbsp;? Et pourtant cet exercice de remise en question du capitalisme a déjà été effectué entre la fin des années 1960 et les années 1980. Cette histoire est racontée par Fred
Turner, dans son excellent livre *Aux sources de l'utopie numérique. De la contre-culture à la cyberculture, Stewart Brand un homme d'influence*[@turner2012]. Dans ce livre,
Fred Turner montre comment les mouvements communautaires de
contre-culture ont soit échoué par désillusion, soit se sont recentrés
(surtout dans les années 1980) autour de *techno-valeurs*, en
particulier portées par des leaders charismatiques géniaux à la manière
de Steve Jobs un peu plus tard. L'idée dominante est que la
revendication politique a échoué à bâtir un monde meilleur&nbsp;; c'est en
apportant des solutions techniques que nous serons capables de résoudre
nos problèmes.

Ne crachons pas dans la soupe&nbsp;! Certains principes qui nous permettent
aujourd'hui de surfer et d'aller sur Wikipédia, sont issus de ce
mouvement intellectuel. Prenons par exemple [Ted Nelson](https://fr.wikipedia.org/wiki/Ted_Nelson), qui n'a
rien d'un informaticien et tout d'un sociologue. Au milieu des années
1960, il invente le terme *hypertext* par lequel il entend la
possibilité de lier entre eux des documents à travers un réseau
documentaire. Cela sera formalisé par la suite en système hypermédia
(notamment avec le travail de Douglas Engelbart). Toujours est-il que
Ted Nelson, en fondant le [projet Xanadu](https://fr.wikipedia.org/wiki/Projet_Xanadu) a
proposé un modèle économique d'accès à la documentation et de partage
d'information (pouvoir par exemple acheter en ligne tel ou tel document
et le consulter en lien avec d'autres). Et il a fallu attendre dix ans
plus tard l'Internet de papa pour développer de manière concrète ce qui
passait auparavant pour des utopies et impliquait un changement radical
de modèle (ici, le rapport livresque à la connaissance, remplacé par une
appropriation hypertextuelle des concepts).

La conversion des hippies de la contre-culture nord-américaine (et
d'ailleurs aussi) au techno-capitalisme ne s'est donc pas faite à partir
de rien. Comme bien souvent en histoire des techniques, c'est la
convergence de plusieurs facteurs qui fait naître des changements
technologiques. Ici les facteurs sont&nbsp;:

- des concepts travaillés théoriquement comme l'*hypertext*,
  l'homme augmenté, l'intelligence artificielle, et tout ce qui sera par
  la suite dénommé «&nbsp;cyber-culture&nbsp;»,
- des innovations informatiques&nbsp;: les réseaux (par exemple, la
  commutation de paquets), les micro-computers et les systèmes
  d'exploitation, les langages informatiques (Lisp, C, etc.),
- des situations politiques et économiques particulières&nbsp;: la guerre
  froide (développement des réseaux et de la recherche informatique),
  les déréglementations des politiques néolibérales américaines et
  anglaises, notamment, qui permirent une expansion des marchés
  financiers et donc l'émergence de start-ups dont la Silicon Valley est
  l'emblème le plus frappant, puis plus tard l'arrivée de la «&nbsp;bulle Internet&nbsp;», etc.


## Le logiciel libre montre la  faille

Pour tout changement technologique, il faut penser les choses de manière
globale. Ne jamais se contenter de les isoler comme des émergences
sporadiques qui auraient révolutionné tout un contexte par un jeu de
dominos. Parfois les enchaînements n'obéissent à aucun enchaînement
historique linéaire et ne sont dus qu'au hasard d'un contexte favorable
qu'il faut identifier. On peut se demander quelle part favorable de ce
contexte intellectuellement stimulant a permis l'émergence de concepts
révolutionnaires, et quelle part a mis en lumière les travers de la
«&nbsp;contre-culture dominante&nbsp;» de la future Silicon Valley.

Tel l'œuf et la poule, on peut parler du
[logiciel libre](https://fr.wikipedia.org/wiki/Logiciel_libre) dont
le concept a été formalisé par Richard Stallman[@stallman2010]. Ce dernier évoluait
dans le milieu high-tech du M.I.T. durant cette période qui vit
l'émergence des hackers &ndash;&nbsp;voir sur ce point le livre de Steven Lévy,
 *L'Éthique des hackers*[@Levy2013, p. 169 et suiv.]. Dans le
groupe de hackers dont R. Stallman faisait partie, certains se
disputaient les programmes de Machines Lisp[@stallman2010, chap. 7]. Deux sociétés avaient été fondées, distribuant ce types de
machines. Le marché tacite qui était passé était de faire en sorte que
tous les acteurs (l'université et ces entreprises) puissent profiter des
améliorations apportées par les uns et les autres. Néanmoins, une firme
décida de ne plus partager, accusant l'université de fausser la
concurrence en reversant ses améliorations à l'autre entreprise. Cet
évènement, ajouté à d'autres épisodes aussi peu glorieux dans le monde
des hackers, amena Richard Stallman à formaliser juridiquement l'idée
qu'un programme puisse non seulement être partagé et amélioré par tous,
mais aussi que cette caractéristique puisse être virale et toucher
toutes les améliorations et toutes les versions du programme.

En fait, le logiciel libre est doublement révolutionnaire. Il s'oppose
en premier lieu à la logique qui consiste à privatiser la connaissance
et l'usage pour maintenir les utilisateurs dans un état de dépendance et
établir un monopole basé uniquement sur le non-partage d'un programme
(c'est à dire du code issu d'un langage informatique la plupart du temps
connu de tous). Mais surtout, il s'oppose au contexte dont il est
lui-même issu&nbsp;: un monde dans lequel on pensait initier des modèles
économiques basés sur la capacité de profit que représente l'innovation
technologique. Celle-ci correspondant à un besoin, elle change le monde
mais ne change pas le modèle économique de la génération de profit par
privatisation de la production. Or, avec le logiciel libre,
l'innovation, ou plutôt le programme, ne change rien de lui-même&nbsp;: il
est appelé à être modifié, diffusé et amélioré par tous les utilisateurs
qui, parce qu'ils y participent, améliorent le monde et partagent à la
fois l'innovation, son processus et même le domaine d'application du
programme. Et une fois formalisé en droit par le concept de licence
libre, cela s'applique désormais à bien d'autres productions que les
seuls programmes informatiques&nbsp;: les connaissances, la musique, la
vidéo, le matériel, etc.

Le logiciel libre représente un épisode divergent dans le *story telling* des Silicon Valley et des Steve Jobs du monde entier. Il
correspond à un moment où, dans cette logique de conversion des
techno-utopistes au capitalisme, la principale faille fut signalée&nbsp;:
l'aliénation des utilisateurs des technologies. Et le remède fut
compris&nbsp;: pour libérer les utilisateurs, il faut qu'il participent
eux-mêmes en partageant les connaissances et la contrepartie de ce
partage est l'obligation du partage. Et on sait, depuis lors, que ce
modèle alternatif n'empêche nullement le profit puisque celui-ci se loge
dans l'exercice des connaissances (l'expertise est monnayable) ou dans
la plus-value elle-même de programmes fortement innovants qui permettent
la production de services ou de nouveaux matériels, etc.

## Économie du partage&nbsp;?

Il faut comprendre, néanmoins, que les fruits de l'intellectualisme
californien des seventies n'étaient pas tous intentionnellement tournés
vers le dieu Capital, un peu à la manière dont Mark Zuckerberg a fondé
Facebook en s'octroyant cette fonction salvatrice, presque morale, de
facilitateur de lien social. Certains projets, voire une grande
majorité, partaient réellement d'une intention parfaitement en phase
avec l'idéal d'une meilleure société fondée sur le partage et le bien
commun. La plupart du temps, ils proposaient un modèle économique fondé
non pas sur les limitations d'usage (payer pour avoir l'exclusivité de
l'utilisation) mais sur l'adhésion à l'idéologie qui sous-tend
l'innovation. Certains objets techniques furent donc des véhicules
d'idéologie.

Dans les années 1973-1975, le projet
[Community Memory](https://en.wikipedia.org/wiki/Community_Memory) véhiculait de l'idéologie[@doub2016]. Ses fondateurs, en particulier [Lee Felsenstein](https://en.wikipedia.org/wiki/Lee_Felsenstein) (inventeur du premier ordinateur portable en 1981), se retrouvèrent par la suite au [Homebrew Computer Club](https://en.wikipedia.org/wiki/Homebrew_Computer_Club), le club d'informatique de la Silicon Valley naissante parmi lesquels on retrouva plus tard Steve Jobs. La Silicon Valley doit
beaucoup à ce club, qui regroupa, à un moment ou à un autre, l'essentiel
des hackers californiens. Avec la Community Memory, l'idée était de
créer un système de communication non hiérarchisé où les membres
pouvaient partager de l'information de manière décentralisée. Community
Memory utilisait les services de Resource One, une organisation non
gouvernementale crée lors de la crise de 1970 et l'invasion américaine
du Cambodge. Il s'agissait de mettre en place un accès à un calculateur
pour tous ceux qui se reconnaissaient dans le mouvement de
contre-culture de l'époque. Avec ce calculateur (Resource One
Generalized Information Retrieval System &ndash;&nbsp;ROGIRS), des terminaux
maillés sur le territoire américain et les lignes téléphoniques WATTS,
les utilisateurs pouvaient entrer des informations sous forme de texte
et les partager avec tous les membres de la communauté, programmeurs ou
non, à partir de terminaux en accès libre. Il s'agissait généralement de
petites annonces, de mini-tracts, etc. dont les mots-clé permettaient le
classement.

![Community Memory Project 1975 Computer History Museum Mountain View California CC By Sa Wikipedia](imgs/communitymemory.jpg)

Pour Lee Felsenstein, le principal intérêt du projet était de
démocratiser l'usage de l'ordinateur, en réaction au monde universitaire
ou aux élus des grandes entreprises qui s'en réservaient l'usage. Mais
les caractéristiques du projet allaient beaucoup plus loin&nbsp;: pas de
hiérarchie entre utilisateurs, respect de l'anonymat, aucune autorité ne
pouvait hiérarchiser l'information, un accès égalitaire à l'information.
En cela, le modèle s'opposait aux modèles classiques de communication
par voie de presse, elle-même dépendante des orientations politiques des
journaux. Il s'opposait de même à tout contrôle de l'État, dans un
climat de méfiance entretenu par les épisodes de la Guerre du Vietnam ou
le scandale du WaterGate. Ce système supposait donc, pour y accéder, que
l'on accepte&nbsp;:

1. de participer à et d'entrer dans une communauté et
2. que les comptes à rendre se font au sein de cette communauté
  indépendamment de toute structure politique et surtout de l'État. Le
  système de pair à pair s'oppose frontalement à tout moyen de contrôle
  et de surveillance externe.

Quant au fait de payer quelques cents pour écrire (entrer des
informations), cela consistait essentiellement à couvrir les frais
d'infrastructure. Ce don participatif à la communauté finissait de
boucler le cercle vertueux de l'équilibre économique d'un système que
l'on peut qualifier d'humaniste.

Si beaucoup de hackers avaient intégré ce genre de principes et si
Internet fut finalement conçu (pour sa partie non-militaire) autour de
protocoles égalitaires, la part libertarienne et antiétatiste ne fut pas
toujours complètement (et aveuglément) acceptée. L'exemple du logiciel
libre le montre bien, puisqu'il s'agit de s'appuyer sur une justice
instituée et réguler les usages.

Pour autant, combinée aux mécanismes des investissements financiers
propres à la dynamique de la Silicon Valley, la logique du «&nbsp;moins
d'État&nbsp;» fini par l'emporter. En l'absence (volontaire ou non) de
principes clairement formalisés comme ceux du logiciel libre, les
sociétés high-tech entrèrent dans un système concurrentiel dont les
motivations consistaient à concilier le partage communautaire,
l'indépendance vis-à-vis des institutions et la logique de profit. Le
résultat ne pouvait être autre que la création d'un marché privateur
reposant sur la mise en commun des pratiques elles-mêmes productrices de
nouveaux besoins et de nouveaux marchés. Par exemple, en permettant au
plus grand nombre possible de personnes de se servir d'un ordinateur
pour partager de l'information, l'informatique personnelle devenait un
marché ouvert et prometteur. Ce n'est pas par hasard que la fameuse
«&nbsp;Lettre aux hobbyistes&nbsp;» de Bill Gates[@gates1976] fut écrite en premier lieu à l'intention
des membres du Homebrew Computer Club&nbsp;: le caractère mercantile et
(donc) privateur des programmes informatiques est la contrepartie
obligatoire, la conciliation entre les promesses d'un nouveau monde
technologique a-politique et le marché capitaliste qui peut les
réaliser.

C'est de ce bain que sortirent les principales avancées informatiques
des années 1980 et 1990, ainsi que le changement des pratiques qu'elles
finirent par induire dans la société, de la gestion de nos comptes
bancaires, aux programmes de nos téléphones portables.

## Piller le code, imposer des usages

À la fin des années 1990, c'est au nom de ce réalisme capitaliste, que
les promoteurs de l'[Open Source Initiative](https://fr.wikipedia.org/wiki/Open_Source_Initiative)[@perens1999] avaient compris l'importance de maintenir des codes
sources ouverts pour faciliter un terreau commun permettant d'entretenir
le marché. Ils voyaient un frein à l'innovation dans les contraintes des
licences du logiciel libre tel que le proposaient Richard Stallman et la
Free Software Foundation (par exemple, l'obligation de diffuser les
améliorations d'un logiciel libre sous la même licence, comme l'exige la
licence GNU GPL &ndash;&nbsp;General Public License). Pour eux, l'ouverture du
code est une opportunité de création et d'innovation, ce qui n'implique
pas forcément de placer dans le bien commun les résultats produits grâce
à cette ouverture. Pas de *fair play*&nbsp;: on pioche dans le bien
commun mais on ne redistribue pas, du moins, pas obligatoirement.

Les exemples sont nombreux de ces entreprises qui utilisent du code
source ouvert sans même participer à l'amélioration de ce code, voire en
s'octroyant des pans entiers de ce que des généreux programmeurs ont
choisi de verser dans le bien commun. D'autres entreprises trouvent
aussi le moyen d'utiliser du code sous licence libre GNU GPL en y
ajoutant tant de couches successives de code privateur que le système
final n'a plus rien de libre ni d'ouvert. C'est le cas du système
Android de Google, [dont le noyau est Linux](http://www.gnu.org/philosophy/android-and-users-freedom.fr.html)[@stallman2010].

Jamais jusqu'à aujourd'hui le logiciel libre n'avait eu de plus dur
combat que celui non plus de proposer une simple alternative à
informatique privateur, mais de proposer une alternative au modèle
économique lui-même. Pas seulement l'économie de l'informatique, dont on
voudrait justement qu'il ne sorte pas, mais un modèle beaucoup plus
général qui est celui du pillage intellectuel et physique qui aliène les
utilisateurs et, donc, les citoyens. C'est la raison pour laquelle le
discours de Richard Stallman est un discours politique avant tout.

La fameuse dualité entre open source et logiciel libre n'est finalement
que triviale. On n'a pas tort de la comparer à une querelle d'église
même si elle reflète un mal bien plus général. Ce qui est pillé
aujourd'hui, ce n'est plus seulement le code informatique ou les
libertés des utilisateurs à pouvoir disposer des programmes. Même si le
principe est (très) loin d'être encore communément partagé dans
l'humanité, le fait de cesser de se voir imposer des programmes n'est
plus qu'un enjeu secondaire par rapport à une nouvelle voie qui se
dévoile de plus en plus&nbsp;: pour maintenir la pression capitaliste sur un
marché verrouillé par leurs produits, les firmes cherchent désormais à
imposer des comportements. Elles ne cherchent plus à les induire comme
on pouvait dire d'Apple que le design de ses produits provoquait un
effet de mode, une attitude «&nbsp;cool&nbsp;». Non&nbsp;: la stratégie a depuis un
moment déjà dépassé ce stade.

Un exemple révélateur et particulièrement cynique, la population belge
en a fait la terrible expérience à l'occasion des attentats commis à
Bruxelles en mars 2016 par de sombres crétins, au prix de dizaines de
morts et de centaines de blessés. Les médias déclarèrent en choeur,
quelques heures à peine après l'annonce des attentats, que Facebook
déclenchait le «&nbsp;Safety Check&nbsp;». Ce dispositif propre à la firme avait
déjà été éprouvé lors des attentats de Paris en novembre 2015 et
[un article de Slate.fr](http://www.slate.fr/story/110009/attentats-facebook-safety-check-responsabilite)[@Newm2015] en montre bien les enjeux. Avec ce dispositif, les
personnes peuvent signaler leur statut à leurs amis sur Facebook en
situation de catastrophe ou d'attentat. Qu'arrive-t-il si vous n'avez
pas de compte Facebook ou si vous n'avez même pas l'application
installée sur votre smartphone&nbsp;? Vos amis n'ont plus qu'à se consumer
d'inquiétude pour vous.

![Facebook safety check fr](imgs/safetycheck.png)

La question n'est pas tant de s'interroger sur l'utilité de ce
dispositif de Facebook, mais plutôt de s'interroger sur ce que cela
implique du point de vue de nos comportements&nbsp;:


- Le devoir d'information&nbsp;: dans les médias, on avait l'habitude, il y a
  encore peu de temps, d'avoir à disposition un «&nbsp;numéro vert&nbsp;» ou un
  site internet produits par les services publics pour informer la
  population. Avec les attentats de Bruxelles, c'est le «&nbsp;Safety Check&nbsp;»
  qui fut à l'honneur. Ce n'est plus à l'État d'assurer la prise en
  charge de la détresse, mais c'est à Facebook de le faire. L'État, lui,
  a déjà prouvé son impuissance puisque l'attentat a eu lieu, CQFD. On
  retrouve la doctrine du «&nbsp;moins d'État&nbsp;».
- La morale&nbsp;: avec la crainte qu'inspire le contexte du terrorisme
  actuel, Facebook joue sur le sentiment de sollicitude et propose une
  solution technique à un problème moral&nbsp;: ai-je le devoir ou non,
  envers mes proches, de m'inscrire sur Facebook&nbsp;?
- La norme&nbsp;: le comportement normal d'un citoyen est d'avoir un
  smartphone, de s'inscrire sur Facebook, d'être en permanence connecté
  à des services qui lui permettent d'être localisé et tracé. Tout
  comportement qui n'intègre pas ces paramètres est considéré comme
  déviant non seulement du point de vue moral mais aussi, pourquoi pas,
  du point de vue sécuritaire.


Ce cas de figure est extrême mais son principe (conformer les
comportements) concerne des firmes aussi gigantesques que Google, Apple,
Facebook, Amazon, Microsoft (GAFAM) et d'autres encore, parmi les plus
grandes capitalisations boursières du monde. Ces dernières sont
aujourd'hui capables d'imposer des usages et des comportements parce
qu'elles proposent toutes une seule idéologie&nbsp;: leurs solutions
techniques peuvent remplacer plus efficacement les pouvoirs publics à la
seule condition d'adhérer à la communauté des utilisateurs, de «&nbsp;prendre la citoyenneté&nbsp;» Google, Apple, Facebook, Microsoft. Le rêve californien
de renverser le Léviathan est en train de se réaliser.

## Vers le capitalisme de surveillance

Ce mal a récemment été nommé par une professeure de Harvard,
[Shoshana Zuboff](https://en.wikipedia.org/wiki/Shoshana_Zuboff)
dans un article intitulé
«&nbsp;Big Other: Surveillance Capitalism and the Prospects of an Information
Civilization&nbsp;»[@zuboff_big_2015]. S. Zuboff analyse les implications de ce qu'elle nomme
le «&nbsp;capitalisme de surveillance&nbsp;» dans notre société connectée.

L'expression a récemment été reprise par Aral Balkan, dans un texte
traduit sur le Framablog intitulé&nbsp;:
«&nbsp;La nature du soi à l'ère numérique&nbsp;»[@Balkan2016]. A. Balkan interroge l'impact du
capitalisme de surveillance sur l'intégrité de nos identités, à travers
nos pratiques numériques. Pour le citer&nbsp;:

> La Silicon Valley est la version moderne du système colonial d'exploitation bâti par la Compagnie des Indes Orientales, mais elle n'est ni assez vulgaire, ni assez stupide pour entraver les individus avec des chaînes en fer. Elle ne veut pas être propriétaire de votre corps, elle se contente d'être propriétaire de votre avatar. Et maintenant, (&hellip;) plus ces entreprises ont de données sur vous, plus votre avatar est ressemblant, plus elles sont proches d'être votre propriétaire.


C'est exactement ce que démontre S. Zuboff (y compris à travers toute sa
bibliographie). Dans l'article cité, à travers l'exemple des pratiques
de Google, elle montre que la collecte et l'analyse des données
récoltées sur les utilisateurs permet l'émergence de nouvelles formes
contractuelles (personnalisation, expérience immersive, etc) entre la
firme et ses utilisateurs. Cela induit des mécanismes issus de
l'extraction des données qui débouchent sur deux principes&nbsp;:


- la marchandisation des comportements&nbsp;: par exemple, les sociétés
  d'assurance ne peuvent que s'émouvoir des données géolocalisées des
  véhicules&nbsp;;
- le contrôle des comportements&nbsp;: il suffit de penser, par exemple, aux
  applications de e-health promues par des assurances-vie et des
  mutuelles, qui incitent l'utilisateur à marcher X heures par jour.


Dans un article paru dans le *Frankfurter Allgemeine Zeitung* le 24
mars 2016, «&nbsp;The Secrets of Surveillance Capitalism&nbsp;»[@zuboff_secrets_2106], S. Zuboff relate les propos de
l'analyste (de données) en chef d'une des plus grandes firmes de la
Silicon Valley&nbsp;:

> Le but de tout ce que nous faisons est de modifier le comportement des
gens à grande échelle. Lorsqu'ils utilisent nos applications, nous
pouvons enregistrer leurs comportements, identifier les bons et les
mauvais comportements, et développer des moyens de récompenser les bons
et pénaliser les mauvais.


Pour S. Zuboff, cette logique d'accumulation et de traitement des
données aboutit à un projet de surveillance lucrative qui change
radicalement les mécanismes habituels entre l'offre et la demande du
capitalisme classique. En cela, le *capitalisme de surveillance*
modifie radicalement les principes de la concurrence libérale qui
pensait les individus autonomes et leurs choix individuels, rationnels
et libres, censés équilibrer les marchés. Qu'on ait adhéré ou non à
cette logique libérale (plus ou moins utopiste, elle aussi, sur certains
points) le fait est que, aujourd'hui, ce capitalisme de surveillance est
capable de bouleverser radicalement les mécanismes démocratiques.
J'aurai l'occasion de revenir beaucoup plus longuement sur le
capitalisme de surveillance dans les chapitres suivants, mais on peut néanmoins affirmer sans plus d'élément qu'il pose en pratique des questions politiques d'une rare envergure.

Ce ne serait rien, si, de surcroît certains décideurs politiques
n'étaient particulièrement pro-actifs, face à cette nouvelle forme du
capitalisme. En France, par exemple, la première version du projet de
Loi Travail soutenu par la ministre El Khomri en mars 2016 entrait
parfaitement en accord avec la logique du capitalisme de surveillance.
Dans la première version du projet, au chapitre *Adaptation du
droit du travail à l'ère numérique*, l'article 23 portait sur les
plateformes collaboratives telles Uber. Cet article rendait impossible
la possibilité pour les «&nbsp;contributeurs&nbsp;» de Uber (les chauffeurs VTC)
de qualifier leur emploi au titre de salariés de cette firme[@Simonnet2016], ceci afin d'éviter les luttes sociales comme les travailleurs de Californie
 qui se sont retournés contre Uber[@Lauer2015] dans une bataille juridique mémorable. Si le projet de loi El Khomri cherche à éliminer le salariat du rapport entre travail et justice, l'enjeu dépasse largement le seul point de vue
juridique.

Google est l'un des actionnaires majoritaires de Uber, et ce n'est pas
pour rien&nbsp;: d'ici 5 ou 6 ans, nous verrons sans doute les premières
voitures sans chauffeur de Google arriver sur le marché. Dès lors, que
faire de tous ces salariés chauffeurs de taxi&nbsp;? La meilleure manière de
s'en débarrasser est de leur supprimer le statut de salariés&nbsp;: en créant
une communauté de contributeurs Uber à leur propre compte, il devient
possible de se passer des chauffeurs puisque ce métier n'existera plus
(au sens de corporation) ou sera en voie d'extinction. Ce faisant, Uber
fait d'une pierre deux coups&nbsp;: il crée aussi une communauté
d'utilisateurs, habitués à utiliser une plate-forme de service de
voiturage pour accomplir leurs déplacements. Uber connaît donc les
besoins, analyse les déplacements, identifie les trajets et rassemble un
nombre incroyable de données qui prépareront efficacement la venue de la
voiture sans chauffeur de Google. Que cela n'arrange pas l'émission de
pollution et empêche de penser à des moyens plus collectifs de
déplacement n'est pas une priorité (quoique Google a déjà son service de bus[@Marin2014]).

## Il faut dégoogliser&nbsp;!

Parmi les moyens qui viennent à l'esprit pour s'en échapper, on peut se
demander si le capitalisme de surveillance est soluble dans la bataille
pour le chiffrement qui refait surface à l'occasion des vagues
terroristes successives. L'idée est tentante&nbsp;: si tous les utilisateurs
étaient en mesure de chiffrer leurs communications, l'extraction de
données de la part des firmes n'en serait que contrariée. Or, la
question du chiffrement n'est presque déjà plus d'actualité que pour les
représentants politiques en mal de sensations. Tels certains ministres
qui ressassent le sempiternel refrain selon lequel le chiffrement permet
aux terroristes de communiquer.

Outre le fait que la pratique «&nbsp;terroriste&nbsp;» du chiffrement reste
encore largement à prouver[@Fradin2016], on se rappelle la bataille récente entre le
FBI et Apple dans le cadre d'une enquête terroriste, où le FBI
souhaitait obtenir de la part d'Apple un moyen (exploitation de
*backdoor*) de faire sauter le chiffrement d'un IPhone. Le FBI
ayant finalement trouvé une solution alternative[@AFP2016], que nous apprend cette dispute&nbsp;? Certes, Apple
veut garder la confiance de ses utilisateurs. Certes le chiffrement a
bien d'autres applications bénéfiques, en particulier lorsqu'on consulte
à distance son compte en banque ou que l'on transfère des données
médicales. Dès lors, la mesure est vite prise entre d'un côté des
gouvernements cherchant à déchiffrer des communications (sans même être
sûr d'y trouver quoique ce soit d'intéressant) et la part gigantesque de
marché que représente le transfert de données chiffrées. Peu importe ce
qu'elles contiennent, l'essentiel est de comprendre non pas ce qui est
échangé, mais qui échange avec qui, pour quelles raisons, et comment
s'immiscer en tant qu'acteur de ces échanges. Ainsi, encore un exemple
parmi d'autres, Google a déployé depuis longtemps des systèmes de
consultations médicale à distance, chiffrées bien entendu&nbsp;: «&nbsp;si vous
voulez un tel service, nous sommes capables d'en assurer la sécurité,
contrairement à un État qui veut déchiffrer vos communications&nbsp;». Le
chiffrement est un élément essentiel du capitalisme de surveillance,
c'est pourquoi Apple y tient tant&nbsp;: il instaure un degré de confiance et
génère du marché.

Nous pourrions passer en revue plusieurs systèmes alternatifs qui
permettraient de s'émanciper plus ou moins du capitalisme de
surveillance. Les solutions ne sont pas uniquement techniques&nbsp;: elles
résident dans le degré de connaissance des enjeux de la part des
populations. Il ne suffit pas de ne plus utiliser les services de
courriel de Google, surtout si on apprécie l'efficacité de ce service.
Il faut se poser la question&nbsp;: «&nbsp;si j'ai le choix d'utiliser ou non
Gmail, dois-je pour autant imposer à mon correspondant qu'il entre en
relation avec Google en me répondant à cette adresse&nbsp;?&nbsp;». C'est
exactement le même questionnement qui s'impose lorsque j'envoie un
document en format Microsoft en exigeant indirectement de mon
correspondant qu'il ait lui aussi le même logiciel pour l'ouvrir.

L'enjeu est éthique. Dans la [Règle d'Or](https://fr.wikipedia.org/wiki/\%C3\%89thique_de_r\%C3\%A9ciprocit\%C3\%A9), c'est la réciprocité qui est importante («&nbsp;ne fais pas à autrui ce que tu n'aimerais pas qu'on te fasse&nbsp;»). Appliquer cette règle à nos rapports technologiques permet une prise de conscience, l'évaluation des enjeux qui dépasse le seul rapport individuel, nucléarisé, que j'ai avec mes pratiques numériques et que le capitalisme de surveillance cherche à m'imposer. Si je choisis d'installer sur mon smartphone l'application de géolocalisation que m'offre mon assureur en guise de test contre un avantage quelconque, il faut que je prenne conscience que je participe
directement à une mutation sociale qui imposera des comportements pour
faire encore davantage de profits. C'est la même logique à l'œuvre avec
l'ensemble des solutions gratuites que nous offrent les GAFAM, à
commencer par le courrier électronique, le stockage en ligne, la
cartographie et la géolocalisation.

Faut-il se passer de toutes ces innovations&nbsp;? Bien sûr que non&nbsp;! le
retranchement anti-technologique n'est jamais une solution. D'autant
plus qu'il ne s'agit pas non plus de dénigrer les grands bienfaits
d'Internet. Par contre, tant que subsisteront dans l'ADN d'Internet les
concepts d'ouverture et de partage, il sera toujours possible de
proposer une alternative au capitalisme de surveillance. Comme le phare
console le marin dans la brume, le logiciel libre et les modèles
collaboratifs qu'il véhicule représentent l'avenir de nos libertés. Nous
ne sommes plus libres si nos comportements sont imposés. Nous ne sommes
pas libres non plus dans l'ignorance technologique.

Il est donc plus que temps, à l'instar de Framasoft, de[Dégoogliser Internet](http://degooglisons-internet.org/) en
proposant non seulement d'autres services alternatifs et respectueux des
utilisateurs, mais surtout les moyens de multiplier les solutions et
décentraliser les usages. Car ce qu'il y a de commun entre le
capitalisme classique et le capitalisme de surveillance, c'est le besoin
centralisateur, qu'il s'agisse des finances ou des données. Ah&nbsp;! si tous
les CHATONS du monde pouvaient se donner la patte&hellip;

[^chatons]: CHATONS est le Collectif des Hébergeurs Alternatifs,Transparents, Ouverts, Neutres et Solidaires. Il rassemble des structures souhaitant éviter la collecte et la centralisation des données personnelles au sein de silos numériques du type de ceux proposés par les GAFAM (Google, Apple, Facebook, Amazon, Microsoft). Voir le site chatons.org.


